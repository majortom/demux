CC=gcc
CFLG=-O -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64
SRC=kb.c demux.c
HED=kb.h demux.h
OBJ=kb.o demux.o

BIND=/usr/local/bin/
INCLUDE=-I linux/include

TARGET=demux

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(CFLG) $(OBJ) -o $(TARGET) $(CLIB) -lm

$(OBJ): $(HED)

install: all
	cp $(TARGET) $(BIND)

uninstall:
	rm $(BIND)$(TARGET)

clean:
	rm -f $(OBJ) $(TARGET) *~ ._*

%.o: %.c
	$(CC) $(CFLG) $(INCLUDE) -c $< -o $@
